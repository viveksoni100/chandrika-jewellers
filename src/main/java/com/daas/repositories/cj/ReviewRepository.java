package com.daas.repositories.cj;

import com.daas.entities.CJ.Review;
import com.daas.entities.DaaS.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Long> {

    List<Review> findAllByProducts(Product product);

}
