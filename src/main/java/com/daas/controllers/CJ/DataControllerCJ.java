package com.daas.controllers.CJ;

import com.daas.entities.DaaS.Product;
import com.daas.entities.DaaS.ProductCategory;
import com.daas.entities.DaaS.ProductCategoryDetail;
import com.daas.entities.DaaS.ProductType;
import com.daas.repositories.daas.ProductCategoryDetailRepository;
import com.daas.repositories.daas.ProductCategoryRepository;
import com.daas.repositories.daas.ProductRepository;
import com.daas.repositories.daas.ProductTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/categories")
public class DataControllerCJ {

    @Autowired
    private ProductTypeRepository productTypeRepository;

    @Autowired
    private ProductCategoryDetailRepository productCategoryDetailRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductRepository productRepository;

    //categories
    @GetMapping(value = {"new"})
    public String newProducts(ModelMap model, HttpServletRequest request) {

        ProductType productType = productTypeRepository.findFirstByProductTypeName("Silver Jewellery");
        List<ProductCategoryDetail> productCategories = productCategoryDetailRepository.findAllByProductTypeAndStatusIsTrue(productType);
        List<Product> productList = new ArrayList<Product>();
        for (ProductCategoryDetail p : productCategories) {
            List<Product> product = new ArrayList<>();
            product = productRepository.findAllByProductCategoryDetailAndNewProductAndStatus(p, true, true);
            productList.addAll(product);
        }

        model.addAttribute("newProductsData", productList);

        return "chandrika/categories/new-main";
    }

    @GetMapping(value = {"bestsellers"})
    public String bestsellers(ModelMap model, HttpServletRequest request) {

        ProductType productType = productTypeRepository.findFirstByProductTypeName("Silver Jewellery");
        List<ProductCategoryDetail> productCategories = productCategoryDetailRepository.findAllByProductTypeAndStatusIsTrue(productType);
        List<Product> productList = new ArrayList<Product>();
        for (ProductCategoryDetail p : productCategories) {
            List<Product> product = new ArrayList<>();
            product = productRepository.findAllByProductCategoryDetailAndBestSellersAndStatus(p, true, true);
            productList.addAll(product);
        }

        model.addAttribute("bestSellersProducts", productList);

        return "chandrika/categories/best-sellers-main";
    }

    @GetMapping(value = {"earrings"})
    public String earrings(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Earrings");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("earringData", productList);

        return "chandrika/categories/earrings-main";
    }

    @GetMapping(value = {"necpen"})
    public String necpen(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Necklace & Pendants");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("necpenData", productList);

        return "chandrika/categories/necpen-main";
    }

    @GetMapping(value = {"anklet"})
    public String anklet(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Anklets");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("ankletsData", productList);

        return "chandrika/categories/anklets-main";
    }

    @GetMapping(value = {"bracelets"})
    public String bracelets(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Bracelets");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("braceletsData", productList);

        return "chandrika/categories/bracelets-main";
    }

    @GetMapping(value = {"jsets"})
    public String jewellerySets(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Jewellery Sets");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("jewellerySetsData", productList);

        return "chandrika/categories/jsets-main";
    }

    @GetMapping(value = {"rings"})
    public String rings(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Rings");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("ringsData", productList);

        return "chandrika/categories/rings-main";
    }

    @GetMapping(value = {"back-in-stock"})
    public String backInStock(ModelMap model, HttpServletRequest request) {

        ProductType productType = productTypeRepository.findFirstByProductTypeName("Silver Jewellery");
        List<ProductCategoryDetail> productCategories = productCategoryDetailRepository.findAllByProductTypeAndStatusIsTrue(productType);
        List<Product> productList = new ArrayList<Product>();
        for (ProductCategoryDetail p : productCategories) {
            List<Product> product = new ArrayList<>();
            product = productRepository.findAllByProductCategoryDetailAndBackInStockAndStatus(p, true, true);
            productList.addAll(product);
        }

        model.addAttribute("backInStockProducts", productList);

        return "chandrika/categories/back-in-stock-main";
    }

    //gifts
    @GetMapping(value = {"jsetsForGifting"})
    public String jsetsForGifting(ModelMap model, HttpServletRequest request) {

        ProductType productType = productTypeRepository.findFirstByProductTypeName("Silver Jewellery");
        List<ProductCategoryDetail> productCategories = productCategoryDetailRepository.findAllByProductTypeAndStatusIsTrue(productType);
        List<Product> productList = new ArrayList<Product>();
        for (ProductCategoryDetail p : productCategories) {
            List<Product> product = new ArrayList<>();
            product = productRepository.findAllByProductCategoryDetailAndJsForGiftingAndStatus(p, true, true);
            productList.addAll(product);
        }

        model.addAttribute("jsetsForGiftData", productList);

        return "chandrika/gifts/jsets4-gifting-main";
    }

    @GetMapping(value = {"mostGifted"})
    public String mostGifted(ModelMap model, HttpServletRequest request) {

        ProductType productType = productTypeRepository.findFirstByProductTypeName("Silver Jewellery");
        List<ProductCategoryDetail> productCategories = productCategoryDetailRepository.findAllByProductTypeAndStatusIsTrue(productType);
        List<Product> productList = new ArrayList<Product>();
        for (ProductCategoryDetail p : productCategories) {
            List<Product> product = new ArrayList<>();
            product = productRepository.findAllByProductCategoryDetailAndMostGiftedAndStatus(p, true, true);
            productList.addAll(product);
        }

        model.addAttribute("mostGiftedData", productList);

        return "chandrika/gifts/most-gifted-main";
    }

    @GetMapping(value = {"coupleJewel"})
    public String coupleJewellery(ModelMap model, HttpServletRequest request) {

        ProductType productType = productTypeRepository.findFirstByProductTypeName("Silver Jewellery");
        List<ProductCategoryDetail> productCategories = productCategoryDetailRepository.findAllByProductTypeAndStatusIsTrue(productType);
        List<Product> productList = new ArrayList<Product>();
        for (ProductCategoryDetail p : productCategories) {
            List<Product> product = new ArrayList<>();
            product = productRepository.findAllByProductCategoryDetailAndCoupleJewelleryAndStatus(p, true, true);
            productList.addAll(product);
        }

        model.addAttribute("coupleJewelData", productList);

        return "chandrika/gifts/couple-jewel-main";
    }

    @GetMapping(value = {"below2k"})
    public String below2k(ModelMap model, HttpServletRequest request) {

        ProductType productType = productTypeRepository.findFirstByProductTypeName("Silver Jewellery");
        List<ProductCategoryDetail> productCategories = productCategoryDetailRepository.findAllByProductTypeAndStatusIsTrue(productType);
        List<Product> productList = new ArrayList<Product>();
            List<Product> product = new ArrayList<>();
            product = productRepository.findAllBelow2kPricedProducts();
            productList.addAll(product);

        model.addAttribute("below2kData", productList);

        return "chandrika/gifts/below2k-main";
    }

    @GetMapping(value = {"above2k"})
    public String abov2k(ModelMap model, HttpServletRequest request) {

        ProductType productType = productTypeRepository.findFirstByProductTypeName("Silver Jewellery");
        List<ProductCategoryDetail> productCategories = productCategoryDetailRepository.findAllByProductTypeAndStatusIsTrue(productType);
        List<Product> productList = new ArrayList<Product>();
        List<Product> product = new ArrayList<>();
        product = productRepository.findAllAbove2kPricedProducts();
        productList.addAll(product);

        model.addAttribute("above2kData", productList);

        return "chandrika/gifts/above2k-main";
    }

    //collections
    @GetMapping(value = {"preciousPearl"})
    public String preciousPearl(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Precious Pearl");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("preciousPearlData", productList);

        return "chandrika/collections/prec-pearl-main";
    }

    @GetMapping(value = {"roseGold"})
    public String roseGold(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Rose Gold");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("roseGoldData", productList);

        return "chandrika/collections/rose-gold-main";
    }

    @GetMapping(value = {"sparklingSilver"})
    public String sparklingSilver(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Sparkling Silver");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("sparklingSilverData", productList);

        return "chandrika/collections/spark-silver-main";
    }

    @GetMapping(value = {"cupidsLove"})
    public String cupidsLove(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Cupid's Love");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("cupidsLoveData", productList);

        return "chandrika/collections/cupid-love-main";
    }

    @GetMapping(value = {"moonDanglers"})
    public String moonDanglers(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Moonlight Danglers");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("moonDanglersData", productList);

        return "chandrika/collections/moon-danglers-main";
    }

    @GetMapping(value = {"radiantDiva"})
    public String radiantDiva(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Radiant Diva");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("radiantDivaData", productList);

        return "chandrika/collections/radiant-diva-main";
    }

    @GetMapping(value = {"sparkNature"})
    public String sparkNature(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Sparkling Nature");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("sparkNatureData", productList);

        return "chandrika/collections/spark-nature-main";
    }

    @GetMapping(value = {"twinkleStars"})
    public String twinkleStars(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Twinkle Stars");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("twinkleStarsData", productList);

        return "chandrika/collections/twinkle-stars-main";
    }

    @GetMapping(value = {"waltHeart"})
    public String waltHeart(ModelMap model, HttpServletRequest request) {

        ProductCategory productCategory = productCategoryRepository.findFirstByProductCategoryName("Waltzing Hearts");
        ProductCategoryDetail productCategoryDetail = productCategoryDetailRepository.findAllByProductCategoryAndIsDeletedAndStatus(productCategory, false, true);
        List<Product> productList = new ArrayList<Product>();
        productList = productRepository.findAllByProductCategoryDetailAndIsDeletedAndStatus(productCategoryDetail, false, true);

        model.addAttribute("waltHeartData", productList);

        return "chandrika/collections/walt-heart-main";
    }


}
