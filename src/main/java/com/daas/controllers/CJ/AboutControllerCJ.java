package com.daas.controllers.CJ;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/about")
public class AboutControllerCJ {

    @GetMapping(value = {"jcare"})
    public String jewelleryCare(ModelMap model, HttpServletRequest request) {

        return "chandrika/about/jewellery-care.html";
    }

    @GetMapping(value = {"offers"})
    public String offers(ModelMap model, HttpServletRequest request) {

        return "chandrika/about/offers.html";
    }

    @GetMapping(value = {"contactUs"})
    public String contactUs(ModelMap model, HttpServletRequest request) {

        return "chandrika/about/contact-us.html";
    }

    @GetMapping(value = {"sendFeedback"})
    public String sendFeedback(ModelMap model, HttpServletRequest request) {

        return "chandrika/about/send-feedback.html";
    }

    @GetMapping(value = {"joinUs"})
    public String joinUs(ModelMap model, HttpServletRequest request) {

        return "chandrika/about/join-us.html";
    }

}
