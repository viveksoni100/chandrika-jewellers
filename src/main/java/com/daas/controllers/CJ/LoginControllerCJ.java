package com.daas.controllers.CJ;

import com.daas.alerts.Alerts;
import com.daas.controllers.IndexController;
import com.daas.controllers.iface.BaseControllerIface;
import com.daas.entities.user.ForgotPasswordToken;
import com.daas.entities.user.User;
import com.daas.events.event.ForgotPasswordEvent;
import com.daas.repositories.iface.user.ForgotPasswordRepository;
import com.daas.repositories.iface.user.UserRepository;
import com.daas.utilities.singleton.CommonUtil;
import com.daas.utilities.singleton.LocaleHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping("/signin")
public class LoginControllerCJ {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ForgotPasswordRepository forgotPasswordRepository;

    @Autowired
    protected Environment environment;

    @Autowired
    protected ApplicationEventPublisher eventPublisher;

    @Autowired
    protected LocaleHelper localeHelper;

    @Autowired
    protected Alerts alerts;

    public final static Logger LOG = LogManager.getLogger(LoginControllerCJ.class.getName());

    @GetMapping(value = {"login-page"})
    public String loginPage(ModelMap model, HttpServletRequest request) {

        return "chandrika/login/login-page-main.html";
    }

    @GetMapping(value = {"register-page"})
    public String registerPage(ModelMap model, HttpServletRequest request) {

        return "chandrika/login/register-page-main.html";
    }

    @GetMapping(value = {"forgot-password"})
    public String forgotPassword(ModelMap model, HttpServletRequest request) {

        return "chandrika/login/forgot-pass-main.html";
    }

    @GetMapping(value = {"password-reset"})
    public String passwordReset(ModelMap model, @RequestParam("email") String email, RedirectAttributes redirectAttributes) {
        try {
            if (email != null && CommonUtil.isEmailIdValid(email)) {
                User user = userRepository.findFirstByEmail(email);
                if (user != null) {
                    ForgotPasswordToken forgotPasswordToken = forgotPasswordRepository.findFirstByUser(user);
                    LocalDateTime expiryDateTime = LocalDateTime.now().plusMinutes(Long.parseLong(environment.getProperty("forgot.password.expiry.minutes")));
                    if (forgotPasswordToken == null) {
                        forgotPasswordToken = new ForgotPasswordToken();
                        forgotPasswordToken.setUser(user);
                    }
                    forgotPasswordToken.setToken(CommonUtil.generateToken());
                    forgotPasswordToken.setExpiryDate(expiryDateTime);
                    forgotPasswordToken.setStatus(true);
                    forgotPasswordRepository.save(forgotPasswordToken);
                    eventPublisher.publishEvent(new ForgotPasswordEvent(user, localeHelper.getCurrentLocale(), forgotPasswordToken));
                }
                alerts.setSuccess("forgot.password.link.sent");
            } else {
                alerts.setError("email.invalid.format");
            }
        } catch (Exception e) {
            LOG.error("Error generating forgot password link ", e);
        }
        alerts.setAlertModelAttribute(model);
        alerts.setAlertRedirectAttribute(redirectAttributes);
        alerts.clearAlert();
        return "chandrika/login/forgot-pass-main.html";
    }

}
