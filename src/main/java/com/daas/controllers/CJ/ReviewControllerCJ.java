package com.daas.controllers.CJ;

import com.daas.components.encrypter.PathVariableEncrypt;
import com.daas.controllers.CorporateController;
import com.daas.entities.CJ.Review;
import com.daas.entities.DaaS.Product;
import com.daas.repositories.cj.ReviewRepository;
import com.daas.repositories.daas.ProductRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/review")
public class ReviewControllerCJ {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    PathVariableEncrypt pathVariableEncrypt;

    public final static Logger LOG = LogManager.getLogger(ReviewControllerCJ.class.getName());

    @PostMapping(value = "/saveReview/{id}")
    public String saveReview(@RequestParam("comment") String comment,
                             @RequestParam("author") String author,
                             @RequestParam("email") String email, ModelMap model,
                             @PathVariable String id) {

        Product product = productRepository.findFirstById(Long.parseLong(pathVariableEncrypt.decrypt(id)));

        Review review = new Review();
        review.setReview(comment);
        review.setNameOfReviewer(author);
        review.setEmailOfReviewer(email);
        review.setProducts(product);

        reviewRepository.save(review);

        return "redirect:/productdetail/" + id;
    }

    @GetMapping(value = "/deleteComment/{id}/{productid}")
    public String deleteComment(@PathVariable String id, @PathVariable String productid) {

        if (id == null) {
            LOG.error("The comment you are trying to delete is containing null id");
        }
        Review review = reviewRepository.findById(Long.parseLong(pathVariableEncrypt.decrypt(id))).get();
        reviewRepository.delete(review);
        return "redirect:/productdetail/" + productid;
    }

}
