package com.daas.controllers.CJ;

import com.daas.entities.DaaS.Product;
import com.daas.entities.DaaS.ProductCategory;
import com.daas.entities.DaaS.ProductCategoryDetail;
import com.daas.entities.DaaS.ProductType;
import com.daas.repositories.daas.ProductCategoryDetailRepository;
import com.daas.repositories.daas.ProductCategoryRepository;
import com.daas.repositories.daas.ProductRepository;
import com.daas.repositories.daas.ProductTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/special")
public class SpecialControllerCJ {

    @GetMapping(value = {"customizedJewellery"})
    public String customizedJewellery(ModelMap model, HttpServletRequest request) {

        return "chandrika/special/customized-jewel-main";
    }

    @GetMapping(value = {"diwali2020"})
    public String diwali2020(ModelMap model, HttpServletRequest request) {

        return "chandrika/special/diwali-2020-special-main";
    }

}
