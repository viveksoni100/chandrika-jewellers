package com.daas.controllers.CJ;

import com.daas.components.encrypter.PathVariableEncrypt;
import com.daas.entities.DaaS.Cart;
import com.daas.entities.DaaS.CartDetail;
import com.daas.entities.DaaS.Product;
import com.daas.entities.user.User;
import com.daas.repositories.daas.CartDetailRepository;
import com.daas.repositories.daas.CartRepository;
import com.daas.repositories.daas.ProductRepository;
import com.daas.utilities.session.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/cart")
public class CartControllerCJ {

    @Autowired
    protected PathVariableEncrypt pathVariableEncrypt;

    @Autowired
    protected ProductRepository productRepository;

    @Autowired
    protected UserUtil userUtil;

    @Autowired
    protected CartRepository cartRepository;

    @Autowired
    protected CartDetailRepository cartDetailRepository;

    @GetMapping(value = "/cartPage")
    public String cartPage(){
        System.out.println("");
        return "chandrika/cart-page";
    }

    @GetMapping(value = {"/addtocartCJ/", "/addtocartCJ/{id}"})
    public String addProductToCartCJ(@PathVariable Optional<String> id,
                                     HttpServletRequest request, ModelMap model, RedirectAttributes redirectAttributes) throws Exception {
        Cart cartForUser = null;
        if (id.isPresent()) {
            Product product = productRepository.findFirstById(Long.valueOf(id.get()));
            CartDetail cartDetail = null;
            User currentUser = userUtil.getCurrentUser();
            if (currentUser != null) {
                if (cartRepository.findByDaasUser(currentUser) != null) {
                    cartForUser = cartRepository.findByDaasUser(currentUser);
                } else {
                    cartForUser = new Cart();
                    cartForUser.setDaasUser(userUtil.getCurrentUser());
                    cartForUser.setSessionId(null);
                    cartRepository.save(cartForUser);
                }
                cartDetail = new CartDetail();
                cartDetail.setProduct(product);
                cartDetail.setQuantity(1l); //this will change
                cartDetail.setTotalPrice(product.getProductPrice());
                cartDetail.setCart(cartForUser);
                cartDetailRepository.save(cartDetail);
            }
        } else {
            throw new Exception("Product id is not present for adding the product in cart");
        }
        return "redirect:/productdetail/" + id.get();
    }

}
