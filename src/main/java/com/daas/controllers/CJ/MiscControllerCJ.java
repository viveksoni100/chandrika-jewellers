package com.daas.controllers.CJ;

import com.daas.alerts.Alerts;
import com.daas.components.encrypter.PathVariableEncrypt;
import com.daas.entities.user.OTP;
import com.daas.entities.user.User;
import com.daas.repositories.iface.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/misc")
public class MiscControllerCJ {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PathVariableEncrypt pathVariableEncrypt;

    @Autowired
    protected Alerts alerts;

    @PostMapping(value = {"/otpFillUp/{id}"})
    public String otpFillUp(@RequestParam("firstChar") String firstChar,
                            @RequestParam("secondChar") String secondChar,
                            @RequestParam("thirdChar") String thirdChar,
                            @RequestParam("fourthChar") String fourthChar, ModelMap model, @PathVariable String id) {
        String ret = "chandrika/login/register-page-main";
        if(id != null) {
            User user = userRepository.findFirstById(Long.parseLong(pathVariableEncrypt.decrypt(id)));
            String otp = firstChar + secondChar + thirdChar + fourthChar;
            if(user.getOneTimePassword().equals(otp)) {
                user.setStatus(true);
                userRepository.save(user);
                alerts.setSuccess("registration.success");
                ret = "chandrika/login/login-page-main";
            } else {
                alerts.setError("wrong.otp");
                model.addAttribute("user", user);
                ret = "chandrika/login/OTP-page-main";
            }
        }
        alerts.setAlertModelAttribute(model);
        alerts.clearAlert();
        return ret;
    }

}
