package com.daas.entities.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OTP {

    private String firstChar;
    private String secondChar;
    private String thirdChar;
    private String fourthChar;

}