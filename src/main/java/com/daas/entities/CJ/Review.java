package com.daas.entities.CJ;

import com.daas.entities.BaseEntity;
import com.daas.entities.DaaS.Product;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "cj_product_review")
public class Review extends BaseEntity {

    @Column(name = "review")
    private String review;
    @Column(name = "name_of_reviewer")
    private String nameOfReviewer;
    @Column(name = "email_of_reviewer")
    private String emailOfReviewer;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product products;

}
