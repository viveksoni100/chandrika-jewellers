package com.daas.utilities;

import com.daas.entities.user.User;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class SMSUtility {

    public static final Logger LOG = LogManager.getLogger(SMSUtility.class.getName());
    private static final String ACC_SID = "AC58bddb010664a9cb38e9fdc71f209b14";
    private static final String ACC_AUTH_KEY = "d00a02715619f09c7a2a59f9e0a24ecd";
    private static final String FROM = "+12057518266";
    private static final String TO = "+918866128862";

    public void sendSms(User user, String msg) {

        try {

            Twilio.init(ACC_SID, ACC_AUTH_KEY);
            Message message = Message.creator(new PhoneNumber(TO),
                    new PhoneNumber(FROM),
                    msg).create();
            LOG.info(message.getSid());

        } catch (Exception e) {
            LOG.error("Error sending message");
        }

    }

}
