alter table daas_product_offer
modify product_offer_description varchar(2000);

alter table daas_product
modify product_description varchar(2000);

ALTER TABLE `publications`.`daas_product_version`
ADD COLUMN `product_version_note` VARCHAR(5000) NULL DEFAULT NULL AFTER `product_id`;

/*2 category add karia 6i a Silver and Gold Jewellery*/
UPDATE `daas_product_type` SET `product_type_name` = 'Silver Jewellery' WHERE `daas_product_type`.`id` = 1;
UPDATE `daas_product_type` SET `product_type_name` = 'Gold Jewellery' WHERE `daas_product_type`.`id` = 2;
UPDATE `daas_product_type` SET `product_type_description` = 'Silver' WHERE `daas_product_type`.`id` = 1;
UPDATE `daas_product_type` SET `product_type_description` = 'Gold' WHERE `daas_product_type`.`id` = 2;


/*garbage*/
/*UPDATE `publications`.`daas_product_type` SET `product_type_name` = 'Customs Data' WHERE (`id` = '1');
UPDATE `publications`.`daas_product_type` SET `product_type_name` = 'Web Service' WHERE (`id` = '2');*/

/*for Chandrika DB*/
ALTER TABLE `daas_product` ADD `jewelleryset_for_gifting` BIT(1) NULL DEFAULT NULL AFTER `is_deleted`;
ALTER TABLE `daas_product` ADD `most_gifted` BIT(1) NULL DEFAULT NULL AFTER `jewelleryset_for_gifting`;
ALTER TABLE `daas_product` ADD `couple_jewellery` BIT(1) NULL DEFAULT NULL AFTER `most_gifted`;

